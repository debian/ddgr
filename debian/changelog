ddgr (2.2-1) unstable; urgency=medium

  * Import New upstream version
  * d/control:
    - Bump Standards-Version to 4.6.2
  * d/copyright:
    - Update copyright info.

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 16 Feb 2024 21:54:55 +0800

ddgr (2.1-1) unstable; urgency=medium

  * Import New upstream version
  * Fix d/watch issue
  * d/control:
    - Bump Standards-Version to 4.6.1.0

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 17 Dec 2022 16:30:01 +0800

ddgr (2.0-1) unstable; urgency=medium

  * Import New upstream version
  * d/copyright:
    - Update the copyright year
  * d/control:
    - Bump Standards-Version to 4.6.0.1

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 05 Mar 2022 15:56:21 +0800

ddgr (1.9-2) unstable; urgency=medium

  * d/tests/test:
    - Mark as superficial (Closes: #971461)

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 29 Oct 2020 11:26:16 +0800

ddgr (1.9-1) unstable; urgency=medium

  * Import New upstream version
  * Tweak patch context
  * Add salsa-ci.yml
  * d/control:
    - Bump debhelper-compat to 13

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 28 Jul 2020 12:03:44 +0800

ddgr (1.8.1-1) unstable; urgency=medium

  * Import New upstream version
  * Tweak patch context

 -- SZ Lin (林上智) <szlin@debian.org>  Fri, 10 Apr 2020 10:23:27 +0800

ddgr (1.7+git20190928.bccdc92-2) unstable; urgency=medium

  * Include zsh and fish completions in package (Closes: #946173)
  * Add autopkgtest test case
  * d/control:
    - Bump Standards-Version to 4.5.0

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 27 Jan 2020 17:15:17 +0800

ddgr (1.7+git20190928.bccdc92-1) unstable; urgency=medium

  * Import New upstream version
  * d/control:
    - Bump Standards-Version to 4.4.1
    - Set Rules-Requires-Root: no

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 08 Oct 2019 15:55:41 +0800

ddgr (1.7-1) UNRELEASED; urgency=medium

  * Import New upstream version
  * d/control:
    - Bump Standards-Version to 4.4.0
  * d/watch:
    - Fix malformed filenamemangle

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 06 Oct 2019 12:02:57 +0800

ddgr (1.6-1) unstable; urgency=medium

  * Import New upstream version
  * d/control:
    - Add 'python3-setuptools' into Build-Depends
  * d/patches:
    - Add "delete unnecessary copyfile function to avoid upstream changes" patch
  * d/rules:
    - Use pybuild as a build system
  * d/upstream/metadata:
    - Update content

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 19 Nov 2018 13:42:36 +0800

ddgr (1.5-1) unstable; urgency=medium

  * Import New upstream version
  * Add upstream metadata
  * d/control:
    - Bump Standards-Version to 4.2.1
    - Bump python3 minimal requirement to 3.5
  * d/rules:
    - Add override_dh_missing target (--fail-missing)

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 11 Sep 2018 09:57:09 +0800

ddgr (1.4-1) unstable; urgency=medium

  * Import New upstream version
  * d/conrol: Move VCS to salsa
  *           Bump Standards-Version to 4.1.3
  *           Bump debhelper to 11
  *           Add Build-Depends: dh-python
  * d/compat: Bump compat to 11

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 05 Apr 2018 23:52:29 +0800

ddgr (1.2-1) unstable; urgency=medium

  * Import New upstream version
  * d/control: Remove Pyhton3 'requests' library dependency
  *            Remove Python 3.3 (reached EOL) support
  *            Bump Standards-Version to 4.1.2

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 09 Dec 2017 16:21:39 +0800

ddgr (1.1-2) unstable; urgency=medium

  * d/control: Add depends with python3-requests

 -- SZ Lin (林上智) <szlin@debian.org>  Thu, 30 Nov 2017 10:27:24 +0800

ddgr (1.1-1) unstable; urgency=medium

  * Import New upstream version

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 29 Nov 2017 22:23:02 +0800

ddgr (1.0-1) unstable; urgency=medium

  * Initial release (Closes: #880945)

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 06 Nov 2017 10:33:29 +0800
